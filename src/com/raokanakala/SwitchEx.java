package com.raokanakala;

import com.raokanakala.utilities.mathutilities;

/**
 * Created by RaoKanakala on 8/24/17.
 */
public class SwitchEx {

    public void SwitchRun(String strInput) {
        String result = "";
        switch (strInput.toLowerCase()) {
            case "sum":
                result = mathutilities.sum("SUM");
                break;
            case "div":
                result = mathutilities.div("DIVISION");
                break;
            case "mul":
                result = mathutilities.mul("MULTIPLICATION");
                break;
            case "mod":
                result = mathutilities.mod("MODULUS");
                break;
            default:
                result = mathutilities.display("DEFAULT");
        }

        System.out.println(result);
    }
}
