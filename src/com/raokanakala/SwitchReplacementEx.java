package com.raokanakala;

import com.raokanakala.utilities.functionmapper;

import java.util.HashMap;
import java.util.function.Function;

/**
 * Created by RaoKanakala on 9/3/17.
 */
public class SwitchReplacementEx {
    public void SwitchReplacementRun(String strInput) {
        HashMap<String, Function<String, String>> functionMapper = functionmapper.CreateFunctionMapper();
        if (functionMapper.containsKey(strInput)) {
            String result = functionMapper.get(strInput).apply(strInput);
            System.out.println(result);
        }
    }
}
