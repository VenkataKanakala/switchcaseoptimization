package com.raokanakala.utilities;

import java.util.HashMap;
import java.util.function.Function;

/**
 * Created by RaoKanakala on 9/3/17.
 */
public class functionmapper {
    public static HashMap<String, Function<String, String>> CreateFunctionMapper() {
        HashMap<String, Function<String, String>> functionMap = new HashMap<>();
        functionMap.put("sum", mathutilities::sum);
        functionMap.put("div", mathutilities::div);
        functionMap.put("mul", mathutilities::mul);
        functionMap.put("mod", mathutilities::mod);
        return functionMap;
    }
}
